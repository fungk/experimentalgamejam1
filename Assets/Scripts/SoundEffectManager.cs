﻿using UnityEngine;
using System.Collections;
using System;

public class SoundEffectManager : MonoBehaviour {

	public GameObject audioSource;

	public AudioClip hit;
	public AudioClip jump;
	public AudioClip punch;
	public AudioClip victory;
	public AudioClip start;
	public AudioClip change;

	// Use this for initialization
	void Start () {
		SubscribeToEvents();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnLevelWasLoaded(int level){
		SubscribeToEvents();
	}

	void SubscribeToEvents()
	{
		GameObject flow = GameObject.FindWithTag("Flow");
		if(flow != null)
		{
			flow.GetComponent<FlowController>().OnWin += VictorySound;
			flow.GetComponent<FlowController>().OnStart += StartSound;
		}

		GameObject man = GameObject.FindWithTag("Manager");
		if(man != null)
		{
			man.GetComponent<Stopwatch>().OnChange += ChangeSound;
		}

		GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject go in players)
		{
			PlayerControls pc = go.GetComponent<PlayerControls>();
			pc.OnHit += HitSound;
			pc.OnJump += JumpSound;
			pc.OnPunch += PunchSound;
		}
	}

	public void HitSound(object sender, EventArgs e)
	{
		GameObject go = (GameObject)Instantiate (audioSource, ((GameObject)sender).transform.position, Quaternion.identity);
		AudioSourceController asc = go.GetComponent<AudioSourceController>();
		asc.Initialize (hit);
	}
	
	public void JumpSound(object sender, EventArgs e)
	{
		GameObject go = (GameObject)Instantiate (audioSource, ((GameObject)sender).transform.position, Quaternion.identity);
		AudioSourceController asc = go.GetComponent<AudioSourceController>();
		asc.Initialize (jump);
	}
	
	public void PunchSound(object sender, EventArgs e)
	{
		GameObject go = (GameObject)Instantiate (audioSource, ((GameObject)sender).transform.position, Quaternion.identity);
		AudioSourceController asc = go.GetComponent<AudioSourceController>();
		asc.Initialize (punch);
	}
	
	public void StartSound(object sender, EventArgs e)
	{
		GameObject go = (GameObject)Instantiate (audioSource, Vector3.zero, Quaternion.identity);
		AudioSourceController asc = go.GetComponent<AudioSourceController>();
		asc.Initialize (start);
	}
	
	public void VictorySound(object sender, EventArgs e)
	{
		GameObject go = (GameObject)Instantiate (audioSource, Vector3.zero, Quaternion.identity);
		AudioSourceController asc = go.GetComponent<AudioSourceController>();
		asc.Initialize (victory, true);
	}
	
	public void ChangeSound(object sender, EventArgs e)
	{
		GameObject go = (GameObject)Instantiate (audioSource, Vector3.zero, Quaternion.identity);
		AudioSourceController asc = go.GetComponent<AudioSourceController>();
		asc.Initialize (change);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeyController : MonoBehaviour {



    List<KeyCode> keys = new List<KeyCode>();

	public Sprite[] keySprites;

	Dictionary<KeyCode, Sprite> spriteDict = new Dictionary<KeyCode, Sprite>();


    void Awake () {

        // Add the Alphabet!
        keys.Add(KeyCode.A); keys.Add(KeyCode.B); keys.Add(KeyCode.C);
        keys.Add(KeyCode.D); keys.Add(KeyCode.E); keys.Add(KeyCode.F);
        keys.Add(KeyCode.G); keys.Add(KeyCode.H); keys.Add(KeyCode.I);
        keys.Add(KeyCode.J); keys.Add(KeyCode.K); keys.Add(KeyCode.L);
        keys.Add(KeyCode.M); keys.Add(KeyCode.N); keys.Add(KeyCode.O);
        keys.Add(KeyCode.P); keys.Add(KeyCode.Q); keys.Add(KeyCode.R);
        keys.Add(KeyCode.S); keys.Add(KeyCode.T); keys.Add(KeyCode.U);
        keys.Add(KeyCode.V); keys.Add(KeyCode.W); keys.Add(KeyCode.X);
        keys.Add(KeyCode.Y); keys.Add(KeyCode.Z);

		keys.Add (KeyCode.Alpha1); keys.Add (KeyCode.Alpha2); keys.Add (KeyCode.Alpha3);
		keys.Add (KeyCode.Alpha4); keys.Add (KeyCode.Alpha5); keys.Add (KeyCode.Alpha6);
		keys.Add (KeyCode.Alpha7); keys.Add (KeyCode.Alpha8); keys.Add (KeyCode.Alpha9);
		keys.Add (KeyCode.Alpha0);

		keys.Add (KeyCode.Minus); keys.Add (KeyCode.Equals);
		keys.Add (KeyCode.LeftBracket); keys.Add (KeyCode.RightBracket);
		keys.Add (KeyCode.Semicolon); keys.Add (KeyCode.Quote);
		keys.Add (KeyCode.Comma); keys.Add (KeyCode.Period); keys.Add (KeyCode.Slash);

		for(int i = 0; i < keys.Count; i++)
		{
			spriteDict.Add (keys[i], keySprites[i]);
		}
    }

    // Update is called once per frame
    void Update () {

    }

    // Returns a Random Key from the List of Available Keys
    public KeyCode GetKey() {

        KeyCode key = keys[Random.Range(0, keys.Count)];
        keys.Remove(key);
        return key;

    }

	public Sprite GetSprite(KeyCode key)
	{
		return(spriteDict[key]);
	}

    public void AddKey(KeyCode key) {

        if (!keys.Contains(key))
            keys.Add(key);
    }

}
